using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Playerdata Playerdata;
    [SerializeField] private Rigidbody Rb_player;
     private Vector3 direction;

    private void Start()
    {

       

    }

    private void Update()
    {

        float horizontal = Input.GetAxis("Horizontal");
       

        direction = new Vector3(0, 0, horizontal);

    }


    private void FixedUpdate()
    {

        Rb_player.velocity = (-Vector3.right + direction) * Playerdata.speed;


    }
}
